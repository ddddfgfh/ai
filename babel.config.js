/**
 * @Author: 李修明
 * @Date:   2022-12-28 14:10:42
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-01-03 13:21:50
 */
module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ]
}
