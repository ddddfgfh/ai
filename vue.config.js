/**
 * @Author: 李修明
 * @Date:   2023-01-16 14:10:41
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-02-01 16:39:15
 */
const path = require("path");
const NodePolyfillPlugin = require('node-polyfill-webpack-plugin')


function resolve(dir) {
  return path.join(__dirname, dir);
}

const name = process.env.VUE_APP_TITLE;
const port = process.env.port;

const { defineConfig } = require("@vue/cli-service");

module.exports = defineConfig({
  outputDir: "dist",
  assetsDir: "static",
  publicPath: process.env.NODE_ENV === "production" ? "/" : "/",
  devServer: {
    open: true,
    host: "localhost",
    port: port,
  },
  configureWebpack: {
    name: name,
    resolve: {
      alias: {
        "@": resolve("src"),
      },
    },
    plugins: [new NodePolyfillPlugin()],
  },
  chainWebpack(config) {
    // set svg-sprite-loader
    config.module
      .rule('svg')
      .exclude.add(resolve('src/assets/icons'))
      .end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/assets/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()
  }
});
