/**
 * @Author: Your name
 * @Date:   2022-04-06 10:42:35
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-02-01 18:25:45
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path);
}