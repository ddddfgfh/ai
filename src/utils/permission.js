/**
 * @Author: 李修明
 * @Date:   2023-01-16 08:26:12
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-02-01 18:12:22
 */
import router from "../router";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import { getToken } from "@/utils/auth";

NProgress.configure({ showSpinner: false });

// (中)路由白名单
// (日)ルーティングホワイトリスト
const whiteList = ["/login"];

// (中)可访问路由
// (日)アクセス可能なルーティング (暫定)
const permissionList = [
    "/",
    "/construction",
    "/quotation",
    "/assessment",
    "/user",
    "/cost-node-definitions",
    "/standard-price",
    "/correction-rate",
    "/404"
];

router.beforeEach((to, from, next) => {
  NProgress.start();
  // has token
  if (getToken()) {
    if (to.path === "/login") {
      next({ path: "/" });
      NProgress.done();
    } else {
      if (permissionList.indexOf(to.path) !== -1) {
        next();
      } else {
        next("/404");
        NProgress.done();
      }
      next();
    }
  } else {
    // no token
    if (whiteList.indexOf(to.path) !== -1) {
      next();
    } else {
      next(`/login`);
      NProgress.done();
    }
  }
});

router.afterEach(() => {
  NProgress.done();
});
