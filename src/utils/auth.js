/**
 * @Author: Your name
 * @Date:   2023-01-16 10:42:35
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-02-01 18:12:14
 */
import Cookies from "js-cookie";

const TokenKey = "Token";

export function getToken() {
  return Cookies.get(TokenKey);
}

export function setToken(token) {
  return Cookies.set(TokenKey, token);
}

export function removeToken() {
  return Cookies.remove(TokenKey);
}
