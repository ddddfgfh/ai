/**
 * @Author: 李修明
 * @Date:   2023-01-16 11:46:45
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-02-01 18:23:01
 */
import axios from "axios";
import { MessageBox, Message } from "element-ui";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import { getToken } from "@/utils/auth";

axios.defaults.headers["Content-Type"] = "application/json;charset=utf-8";

// axiosインスタンスの作成
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 8000,
});

// requestストッパ
service.interceptors.request.use(
  (config) => {
    NProgress.start();
    // (中)是否需要设置 token
    // (日)Tokenが必要かどうか
    const isToken = (config.headers || {}).isToken === false;
    if (getToken() && !isToken) {
      config.headers["Authorization"] = "Bearer " + getToken();
    }
    if (config.method === "get" && config.params) {
      let url = config.url + "?";
      for (const propName of Object.keys(config.params)) {
        const value = config.params[propName];
        var part = encodeURIComponent(propName) + "=";
        if (value !== null && typeof value !== "undefined") {
          if (typeof value === "object") {
            for (const key of Object.keys(value)) {
              const params = propName + "[" + key + "]";
              var subPart = encodeURIComponent(params) + "=";
              url += subPart + encodeURIComponent(value[key]) + "&";
            }
          } else {
            url += part + encodeURIComponent(value) + "&";
          }
        }
      }
      url = url.slice(0, -1);
      config.params = {};
      config.url = url;
    } else {
      config.headers["X-Requested-With"] = "XMLHttpRequest";
      config.headers["Content-Type"] = "application/x-www-form-urlencoded";
    }
    return config;
  },
  (error) => {
    console.log(error);
    Promise.reject(error);
    NProgress.done();
  }
);

// responseストッパ
service.interceptors.response.use(
  (res) => {
    NProgress.done();
    const status = res.data.status || 0;
    const msg = res.data.msg;
    if (status === 99) {
      MessageBox.confirm("", "確認", {
        confirmButtonText: "はい",
        cancelButtonText: "いいえ",
        type: "warning",
      }).then(() => {
        location.href = "/index";
      });
    } else if (status !== 0) {
      Message.error({
        title: msg,
      });
      return Promise.reject("error");
    } else {
      return res.data;
    }
  },
  (error) => {
    console.log("err" + error);
    let { message } = error;
    if (message == "Network Error") {
      message = "バックエンドインターフェース接続例外";
    } else if (message.includes("timeout")) {
      message = "システムインタフェースリクエストタイムアウト";
    } else if (message.includes("Request failed with status code")) {
      message = "システムインターフェイス" + message.substr(message.length - 3) + "例外事項";
    }
    Message({
      message: message,
      type: "error",
      duration: 5 * 1000,
    });
    NProgress.done();
    return Promise.reject(error);
  }
);

export default service;
