/**
 * @Author: 李修明
 * @Date:   2023-01-16 11:35:56
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-02-01 18:23:44
 */

 /**
  *(中)表单重置
  *(日)フォームリセット
  * @export
  * @param {object} refName
  */
 export function resetForm(refName) {
    if (this.$refs[refName]) {
      this.$refs[refName].resetFields();
    }
  }