/**
 * @Author: 李修明
 * @Date:   2023-01-16 11:56:47
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-02-01 18:11:44
 */
import Vue from "vue";
import Vuex from "vuex";
import app from "./modules/app";
import user from "./modules/user";
import getters from "./getters";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    app,
    user,
  },
  getters,
});

export default store;
