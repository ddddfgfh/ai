/**
 * @Author: Your name
 * @Date:   2023-01-16 10:42:35
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-02-01 18:11:59
 */
import Cookies from "js-cookie";

const state = {
  sidebar: Cookies.get("sidebarStatus") ? !!+Cookies.get("sidebarStatus") : true,
  device: "desktop",
  size: Cookies.get("size") || "default",
};

const mutations = {
  TOGGLE_SIDEBAR: (state, status) => {
    state.sidebar = !status;
    if (state.sidebar) {
      Cookies.set("sidebarStatus", 1);
    } else {
      Cookies.set("sidebarStatus", 0);
    }
  },
  TOGGLE_DEVICE: (state, device) => {
    state.device = device;
    if (state.device == "desktop") {
        Cookies.set("size", "default")
    }
    else{
        Cookies.set("size", "small")
    }
  },
  SET_SIZE: (state, size) => {
    state.size = size;
    Cookies.set("size", size);
  },
};

const actions = {
  toggleSideBar({ commit }, status) {
    commit("TOGGLE_SIDEBAR", status);
  },
  toggleDevice({ commit }, device) {
    commit("TOGGLE_DEVICE", device);
  },
  setSize({ commit }, size) {
    commit("SET_SIZE", size);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
