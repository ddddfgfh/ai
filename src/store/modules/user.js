/**
 * @Author: 李修明
 * @Date:   2023-01-16 10:42:35
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-02-01 16:17:59
 */
import { login, logout } from "@/api/login";
import { getToken, removeToken, setToken } from "@/utils/auth";

const user = {
  state: {
    token: getToken(),
    id: "",
    name: "ログインユーザー名",
    email: "",
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token;
    },
    SET_ID: (state, id) => {
      state.id = id;
    },
    SET_NAME: (state, name) => {
      state.name = name;
    },
    SET_EMAIL: (state, email) => {
      state.email = email;
    },
  },

  actions: {
    // (中)登录
    // (日)ログイン
    Login({ commit }, userInfo) {
      const username = userInfo.username;
      const password = userInfo.password;
      return new Promise((resolve, reject) => {
        login(username, password)
          .then((res) => {
            setToken("token"); // 暫定
            //   commit("SET_TOKEN", res.result.token);
            //   commit("SET_ID", res.result.id);
            commit("SET_NAME", "admin"); // 暫定
            //   commit("SET_EMAIL", res.result.email);
            resolve();
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    // (中)退出登录
    // (日)ログアウト
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout()
          .then(() => {
            commit("SET_TOKEN", "");
            removeToken();
            resolve();
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
  },
};

export default user;
