/**
 * @Author: 李修明
 * @Date:   2023-01-16 13:34:19
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-02-01 18:11:49
 */
const getters = {
  sidebar: (state) => state.app.sidebar,
  size: (state) => state.app.size,
  device: (state) => state.app.device,
  token: (state) => state.user.token,
  id: (state) => state.user.id,
  name: (state) => state.user.name,
  email: (state) => state.user.email,
};
export default getters;
