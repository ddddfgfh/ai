/**
 * @Author: 李修明
 * @Date:   2023-01-16 14:10:42
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-02-01 18:28:24
 */
import Vue from "vue";
import Cookies from "js-cookie";
import ElementUI from "element-ui";
import "./assets/styles/index.scss";
import locale from "element-ui/lib/locale/lang/ja";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import "./assets/icons";
import "./utils/permission"
import { resetForm } from "@/utils/tools";

Vue.prototype.msgSuccess = function (msg) {
  this.$message({ showClose: true, message: msg, type: "success" });
};

Vue.prototype.msgError = function (msg) {
  this.$message({ showClose: true, message: msg, type: "error" });
};

Vue.prototype.msgWarning = function (msg) {
  this.$message({ showClose: true, message: msg, type: "warning" });
};

Vue.prototype.msgInfo = function (msg) {
  this.$message({ showClose: true, message: msg, type: "info" });
};

Vue.prototype.resetForm = resetForm;

Vue.use(ElementUI, {
  size: Cookies.get("size") || "default",
  locale,
});

Vue.config.productionTip = false;

new Vue({
  el: "#app",
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
