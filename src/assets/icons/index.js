/**
 * @Author: Your name
 * @Date:   2023-01-18 10:42:35
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-02-01 17:33:12
 */
import Vue from "vue";
import SvgIcon from "@/components/SvgIcon"; // svg component

Vue.component("svg-icon", SvgIcon);

const req = require.context("./svg", false, /\.svg$/);
const requireAll = (requireContext) => requireContext.keys().map(requireContext);
requireAll(req);
