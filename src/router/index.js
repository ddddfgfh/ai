/**
 * @Author: 李修明
 * @Date:   2023-01-16 14:14:10
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-02-01 18:10:08
 */
import Vue from "vue";
import VueRouter from "vue-router";

/* Layout */
import Layout from "@/layout";

Vue.use(VueRouter);

/**
 *
 * hidden: true                 // (中)当设置 true 的时候该URL不会在侧边栏中显示
 *                              // (日)trueに設定すると、サイドバーにURLを表示しない
 * name:'router-name'           // (中)URL的名称
 *                              // (日)URLの名称
 * alwaysShow: true             // (中)当Children为1时设置为true，作为根目录显示
 *                              // (日)Childrenが1のとき、ルートディレクトリとして表示するためにtrueを設定する
 * meta : {
    title: 'title'              // (中)URL在侧边栏中显示的名称
 *                              // (日)サイドバーに表示されるURLの名前
    icon: 'svg-name'            // (中)URL的图标   path: src/assets/icons/svg
 *                              // (日)URLのアイコン
  }
 */

const routes = [
  {
    path: "/login",
    name: "login",
    hidden: true,
    component: () => import("../views/login/index.vue"),
  },
  {
    path: "/error",
    name: "error",
    hidden: true,
    component: Layout,
    children: [
      {
        path: "/404",
        component: () => import("../views/error/404.vue"),
        name: "error404",
      },
    ],
  },
  {
    path: "/",
    name: "index",
    hidden: true,
    component: Layout,
    children: [
      {
        path: "/",
        name: "home",
        component: () => import("../views/index/index.vue"),
        meta: { title: "トップページ", icon: "" },
      },
    ],
  },
  {
    path: "/",
    name: "index",
    hidden: false,
    component: Layout,
    children: [
      {
        path: "/construction",
        name: "construction",
        component: () => import("../views/construction/index.vue"),
        meta: { title: "工事情報", icon: "construction" },
      },
    ],
  },
  {
    path: "",
    name: "index",
    hidden: false,
    component: Layout,
    children: [
      {
        path: "/quotation",
        name: "quotation",
        component: () => import("../views/quotation/index.vue"),
        meta: { title: "見積", icon: "quotation" },
      },
    ],
  },
  {
    path: "",
    name: "index",
    hidden: false,
    component: Layout,
    children: [
      {
        path: "/assessment",
        name: "assessment",
        component: () => import("../views/assessment/index.vue"),
        meta: { title: "査定", icon: "assessment" },
      },
    ],
  },
  {
    path: "/master",
    name: "master",
    hidden: false,
    alwaysShow: true,
    component: Layout,
    meta: { title: "マスタメンテナンス", icon: "master" },
    children: [
      {
        path: "/user",
        name: "user",
        component: () => import("../views/user/index.vue"),
        meta: { title: "ユーザー", icon: "" },
      },
      {
        path: "/cost-node-definitions",
        name: "costNodeDefinitions",
        component: () => import("../views/costNodeDefinitions/index.vue"),
        meta: { title: "費用構造", icon: "" },
      },
      {
        path: "/standard-price",
        name: "standardPrice",
        component: () => import("../views/standardPrice/index.vue"),
        meta: { title: "標準単価", icon: "" },
      },
      {
        path: "/correction-rate",
        name: "correctionRate",
        component: () => import("../views/correctionRate/index.vue"),
        meta: { title: "掛け率", icon: "" },
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  routes: routes,
});

export default router;
