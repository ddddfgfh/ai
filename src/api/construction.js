/**
 * @Author: 夏嘉偉
 * @Date:   2023-01-16 14:54:00
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-02-01 17:31:16
 */
import request from "@/utils/request";

/**
 * 工事情報検索
 *
 * @export
 * @param {string} construction_name
 * @param {string} constraction_code
 * @param {string} constraction_building_name
 * @param {string} division_name
 * @param {string} prefecture_name
 * @param {string} city_name
 * @param {string} usage
 * @param {string} building_structure
 * @param {string} settlement_fiscal_year
 * @param {string} field_office_manager
 * @param {string} construction_item_id
 * @param {string} constraction_methods_id
 * @return {json} 
 */
export function getConstructions({construction_name, constraction_code, constraction_building_name,
                             division_name, prefecture_name, city_name, usage, building_structure,
                              settlement_fiscal_year, field_office_manager, construction_item_id, constraction_methods_id}){
  const data = {
    construction_name,
    constraction_code,
    constraction_building_name,
    division_name,
    prefecture_name,
    city_name,
    usage,
    building_structure,
    settlement_fiscal_year,
    field_office_manager,
    construction_item_id,
    constraction_methods_id
  };
  return request({
    url: "/constructions",
    method: "get",
    data: data
  });
}

/**
 * 工事情報編集
 *
 * @export
 * @param {int} id
 * @return {json} 
 */
export function editConstructions(id){
  const data = {
    id,
  };
  return request({
    url: "/constructions/" + id,
    method: "get",
    data: data
  }); 
}


/**
 * 工事情報削除
 *
 * @export
 * @param {array} id
 * @return {json} 
 */
export function deleteConstructions(id){
  const data = {
    id
  };
  return request({
    url: "/constructions" ,
    method: "delete",
    data: data
  }); 
}

/**
 * 工事情報新規登録
 *
 * @export
 * @param {string} construction_name
 * @param {string} constraction_code
 * @param {string} constraction_building_name
 * @param {string} order_type_id
 * @param {string} division_name
 * @param {string} prefecture_name
 * @param {string} city_name
 * @param {string} address
 * @param {datetime} constraction_start_at
 * @param {datetime} constraction_complete_at
 * @param {string} basement_floor
 * @param {string} ground_floor
 * @param {string} p_floor
 * @param {string} designed_id
 * @param {string} usage
 * @param {string} construction_type_id
 * @param {double} site_area
 * @param {double} building_area
 * @param {double} total_area
 * @param {string} building_structure
 * @param {string} field_office_manager
 * @param {int} supervised_by
 * @param {string} quoted_by
 * @param {array} construction_item_methods_buinldings
 * @return {json} 
 */
export function createConstructions({construction_name, constraction_code, constraction_building_name,
                  order_type_id, division_name, prefecture_name, city_name, address, constraction_start_at, constraction_complete_at,
                    basement_floor, ground_floor, p_floor, designed_id, usage, construction_type_id, site_area, building_area, total_area, 
                    building_structure, field_office_manager, supervised_by, quoted_by, construction_item_methods_buinldings}){
  const data ={
    construction_name,
    constraction_code,
    constraction_building_name, 
    order_type_id,
    division_name,
    prefecture_name,
    city_name,
    address,
    constraction_start_at,
    constraction_complete_at,
    basement_floor,
    ground_floor,
    p_floor,
    designed_id,
    usage,
    construction_type_id,
    site_area,
    building_area,
    total_area,
    building_structure,
    field_office_manager,
    supervised_by,
    quoted_by,
    construction_item_methods_buinldings,
  };
  return request({
    url: "/constructions" ,
    method: "post",
    data: data
  }); 
}

/**
 * 工事情報保存
 *
 * @export
 * @param {int} id
 * @param {string} construction_name
 * @param {string} constraction_code
 * @param {string} constraction_building_name
 * @param {string} order_type_id
 * @param {string} division_name
 * @param {string} prefecture_name
 * @param {string} city_name
 * @param {string} address
 * @param {datetime} constraction_start_at
 * @param {datetime} constraction_complete_at
 * @param {string} basement_floor
 * @param {string} ground_floor
 * @param {string} p_floor
 * @param {string} designed_id
 * @param {string} usage
 * @param {string} construction_type_id
 * @param {double} site_area
 * @param {double} building_area
 * @param {double} total_area
 * @param {string} building_structure
 * @param {string} field_office_manager
 * @param {int} supervised_by
 * @param {string} quoted_by
 * @param {array} construction_item_methods_buinldings
 * @return {json} 
 */
export function saveConstructions({id, construction_name, constraction_code, constraction_building_name,
      order_type_id, division_name, prefecture_name, city_name, address, constraction_start_at, constraction_complete_at,
        basement_floor, ground_floor, p_floor, designed_id, usage, construction_type_id, site_area, building_area, total_area, 
        building_structure, field_office_manager, supervised_by, quoted_by, construction_item_methods_buinldings}){
const data ={
      id,
      construction_id: -1,
      construction_name,
      constraction_code,
      constraction_building_name, 
      order_type_id,
      division_name,
      prefecture_name,
      city_name,
      address,
      constraction_start_at,
      constraction_complete_at,
      basement_floor,
      ground_floor,
      p_floor,
      designed_id,
      usage,
      construction_type_id,
      site_area,
      building_area,
      total_area,
      building_structure,
      field_office_manager,
      supervised_by,
      quoted_by,
      construction_item_methods_buinldings,
};
return request({
url: "/constructions/" + id ,
method: "put",
data: data
}); 
}