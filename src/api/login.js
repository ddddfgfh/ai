/**
 * @Author: 李修明
 * @Date:   2023-01-16 14:54:00
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-02-01 17:32:44
 */
import request from "@/utils/request";

/**
 * ログイン
 *
 * @export
 * @param {string} username
 * @param {string} password
 * @return {json} 
 */
export function login(username, password) {
  const data = {
    username,
    password,
  };
  return request({
    url: "/auth/",
    method: "post",
    data: data
  });
}


/**
 * ログアウト
 *
 * @export
 * @return {json} 
 */
export function logout() {
  return request({
    url: "/logout/",
    method: "post"
  });
}
