/**
 * @Author: 李修明
 * @Date:   2023-01-16 09:33:58
 * @Last Modified by:   李修明
 * @Last Modified time: 2023-02-01 17:40:55
 */
import store from "@/store";

const { body } = document;
const WIDTH = 1024;

export default {
  watch: {
    $route(route) {
      if (this.device === "mobile" && this.sidebar) {
        store.dispatch("app/toggleSideBar", false);
      }
    },
  },
  beforeMount() {
    window.addEventListener("resize", this.$_resizeHandler);
  },
  beforeDestroy() {
    window.removeEventListener("resize", this.$_resizeHandler);
  },
  mounted() {
    const isMobile = this.$_isMobile();
    if (isMobile) {
        store.dispatch("app/toggleDevice", "mobile");
        store.dispatch("app/toggleSideBar", true);
      }
  },
  methods: {
    $_isMobile() {
      const rect = body.getBoundingClientRect();
      return rect.width - 1 < WIDTH;
    },
    $_resizeHandler() {
      if (!document.hidden) {
        const isMobile = this.$_isMobile();
        store.dispatch("app/toggleDevice", isMobile ? "mobile" : "desktop");
        store.dispatch("app/toggleSideBar", isMobile);
      }
    },
  },
};
